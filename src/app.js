c=require('chessground');
axios=require('axios')

var cg = c.Chessground(document.getElementById('dirty'));

// start position.
var fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';

// setup board
cg.set(
  {
    orientation: 'white',
    fen: fen,
    movable:{
      color:"white",
      free:false,
    },
    drawable:{
      enabled:false,
      visible:true,
      brushes:
      { thin: {key: 'pg', color: '#15781B', opacity: 0.4, lineWidth: 7} }
    },
    events:{
      change: function(){
      nextmove()
      cg.set({
//          movable:{color:cg.state.turnColor}
         })
        }
    }
  }
)

// nextmove service for next move suggestions. If a move have been made
// see (checks cg.state) - the move is validated and fen is updated
// a lot of side effects going on here.
var nextmove = function(){

  move = ''
  if (cg.state.lastMove){
    move = cg.state.lastMove.join('');

    var movedPiece = cg.state.pieces[cg.state.lastMove[1]];

    if (cg.state.lastMove[1].charAt(1) == '8' || cg.state.lastMove[1].charAt(1) == '1'){
      if (movedPiece.role == 'pawn'){
        move = move + 'q' // promote queen
      }
    }
  }

  axios.get(settings.backend + 'nextmove?FEN=' + fen   + "&move=" + move + '&movetype=' + settings.movetype)
    .then(function(r) {
      fen = r.data.FEN;
      cg.set({'fen':fen})

      setLegalMoves();

      if (r.data.UCI){
        // set score and move suggestions
        if (r.data.UCI.Results){
          s = r.data.UCI.Results[0].Score
          s = Math.round(s/10)
          document.getElementById("score").innerHTML = s/100;
        }

        sfFrom = r.data.UCI.BestMove.substring(0,2)
        sfTo = r.data.UCI.BestMove.substring(2,4)

        cg.setShapes(
          [
            {orig: sfFrom, dest:sfTo, brush:"green"},
            {orig: r.data.from, dest:r.data.to, brush:"red"}
          ]
        )
      }
      document.getElementById("simplescore").innerHTML = r.data.simplescore/100;

      if (r.data.Outcome != "*"){
        // the game is ended. Show outcome
        document.getElementById("result").innerHTML = "<h1>" +  r.data.Method + "</h1>" + "<h2>" + r.data.Outcome + "</h2>";
      }
   if (cg.state.lastMove){

      var movedPiece = cg.state.pieces[cg.state.lastMove[1]];
      console.log(movedPiece)
      console.log(cg.state)
        setTimeout(function(){

          if (movedPiece.color == "white" ){
            cg.move(sfFrom, sfTo)
//            cg.move(r.data.from,r.data.to )
          }else{
            // comment this if you want to play manualy.
            cg.move(r.data.from, r.data.to)
          }

          cg.set({movable:
            {
              color:"white"},
              turnColor:"white",
          })
        }, 1000);
    }else{
      cg.move("d2", "d4")
    }
  })
}

setLegalMoves = function(){
  axios.get(settings.backend + 'legalmoves?FEN=' + fen)
  .then(function(r) {
    cg.set({
      movable:{dests:r.data}
    })
  })
}

nextmove(); // init first move
setLegalMoves();
